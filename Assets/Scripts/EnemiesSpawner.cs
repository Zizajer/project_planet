﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesSpawner : MonoBehaviour
{
    public GameObject SpawnedEnemy;
    public float GenereatedEnemyMovementSpeed;
    public float UpperSpawnDelayTime;
    public float LowerSpawnDelayTime;
    public float MaxDistanceSpawn;
    public float MinDistanceSpawn;

    private void Start()
    {
        Invoke("Spawn", Random.Range(LowerSpawnDelayTime, UpperSpawnDelayTime));
    }
    
    void Spawn()
    {
        Vector3 spawnedPosition = Vector3.zero;
        while (Vector3.Distance(spawnedPosition,Vector3.zero)<MinDistanceSpawn)
        {
            spawnedPosition = new Vector3(Random.Range(-MaxDistanceSpawn, MaxDistanceSpawn),
                Random.Range(-MaxDistanceSpawn, MaxDistanceSpawn), 0);
        }

        GameObject createdEnemy = Instantiate(SpawnedEnemy,spawnedPosition, new Quaternion(0, 0, 0, 0));
        createdEnemy.GetComponent<EnemyController>().MovementSpeed = GenereatedEnemyMovementSpeed;
        Invoke("Spawn", Random.Range(LowerSpawnDelayTime,UpperSpawnDelayTime));
    }
}
