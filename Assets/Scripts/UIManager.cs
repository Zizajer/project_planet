﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public GameObject EndGamePanel;
    public GameObject OnStartText;

    public void DeactivateOnStartText()
    {
        OnStartText.SetActive(false);
    }

    public void ActiveEndGamePanel()
    {
        Time.timeScale = 0;
        EndGamePanel.SetActive(true);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); 
    }
    
}
