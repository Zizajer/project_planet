﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    private static int score;
    public Text ScoreText;

    public int Score { get => score; }

    private void Start()
    {
        score = 0;
    }

    private void Update()
    {
        ScoreText.text = score.ToString();
    }

    public static void IncreaseScore(int amount)
    {
        score += amount;
    }
}
