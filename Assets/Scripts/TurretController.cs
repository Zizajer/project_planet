﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretController : MonoBehaviour
{
    public GameObject Bullet;
    public float BulletSpeed;

    public void Shoot()
    {
        GameObject createdBullet = Instantiate(Bullet, transform.position, transform.parent.rotation);
        createdBullet.transform.Rotate(0,0,-90);
        createdBullet.GetComponent<BulletController>().MovementSpeed = BulletSpeed;
    }

}
