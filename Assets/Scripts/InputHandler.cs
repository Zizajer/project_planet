﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputHandler : MonoBehaviour
{
    public UnityEvent TapReaction;


    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
            if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                if (_GameManager.GameState == GameStates.GameStarted)
                    _GameManager.GameState = GameStates.GamePlaying;
                else if(_GameManager.GameState == GameStates.GamePlaying)
                    TapReaction.Invoke();
            }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (_GameManager.GameState == GameStates.GameStarted)
                _GameManager.GameState = GameStates.GamePlaying;
            else if (_GameManager.GameState == GameStates.GamePlaying)
                TapReaction.Invoke();
        }
    }
}
