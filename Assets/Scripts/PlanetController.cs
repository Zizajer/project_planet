﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetController : MonoBehaviour
{
    public float RotationSpeed;


    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0,0,RotationSpeed*Time.deltaTime);
    }

    public void ChangeDirection()
    {
        RotationSpeed = -RotationSpeed;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Enemy")) {
            Camera.main.gameObject
                .GetComponent<UIManager>()
                .ActiveEndGamePanel();
        }
            
    }
}
