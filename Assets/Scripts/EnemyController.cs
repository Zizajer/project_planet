﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public ParticleSystem ExplosionEffect;
    public float MovementSpeed { get; set; }

    private void Start()
    {
        var dir = Vector3.zero - transform.position;
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.Rotate(0, 0, -90);
    }
    
    void Update()
    {
        transform.position += transform.rotation * new Vector3(0, MovementSpeed, 0);
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Bullet"))
        {
            Destroy(collision.gameObject);
            ParticleSystem particleEffect = Instantiate(ExplosionEffect, transform.position, new Quaternion(0, 0, 0, 0));
            Destroy(particleEffect.gameObject, particleEffect.duration);
            Destroy(gameObject);
            ScoreManager.IncreaseScore(10);
        }

    }
}
