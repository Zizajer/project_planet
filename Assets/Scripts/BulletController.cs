﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public float MovementSpeed { get; set; }
    
    void Update()
    {
        transform.position += transform.rotation * new Vector3(0, MovementSpeed, 0);
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
    
}
