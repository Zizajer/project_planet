﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _GameManager : MonoBehaviour
{
    public float BaseEnemiesSpeedValue;
    public float BasePlanetRotationSpeed;
    public int ScoreStep;
    public float IncrementationOfEnemiesSpeed;
    public float IncrementationOfRotationSpeed;
    private int currentLevelOfIncrementation;
    public static GameStates GameState;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 0;
        GameState = GameStates.GameStarted;
        currentLevelOfIncrementation = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(GetComponent<ScoreManager>().Score/ ScoreStep > currentLevelOfIncrementation)
        {
            currentLevelOfIncrementation = GetComponent<ScoreManager>().Score / ScoreStep;
            GetComponent<EnemiesSpawner>().GenereatedEnemyMovementSpeed = BaseEnemiesSpeedValue 
                + (IncrementationOfEnemiesSpeed * currentLevelOfIncrementation);

            FindObjectOfType<PlanetController>().RotationSpeed = BasePlanetRotationSpeed
                + (IncrementationOfRotationSpeed * currentLevelOfIncrementation);
        }

        if (GameState == GameStates.GamePlaying)
        {
            Time.timeScale = 1;
            GetComponent<UIManager>().DeactivateOnStartText();
        }
        else
            Time.timeScale = 0;
    }
}
